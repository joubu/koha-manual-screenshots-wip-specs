describe("admin/globalprefs/OPACBaseURL", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("OPACBaseURL", function () {
        cy.visit("/cgi-bin/koha/catalogue/detail.pl?biblionumber=1");
        cy.get("#catalogue_detail_biblio").should('be.visible').screenshot("OPACBaseURL");
    });
});
