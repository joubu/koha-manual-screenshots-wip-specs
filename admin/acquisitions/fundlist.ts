describe("admin/acquisitions/fundlist", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("fundlist", function () {
        cy.visit("/cgi-bin/koha/admin/aqbudgets.pl?budget_period_id=1");
        cy.get("main").should('be.visible').screenshot("fundlist");
    });
});
