describe("admin/acquisitions/budgetslist", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("budgetslist", function () {
        cy.visit("/cgi-bin/koha/admin/aqbudgetperiods.pl");
        cy.get("main").should('be.visible').screenshot("budgetslist");
    });
});
