describe("admin/acquisitions/newcurrency", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newcurrency", function () {
        cy.visit("/cgi-bin/koha/admin/currency.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("newcurrency");
    });
});
