describe("admin/acquisitions/newbudgetform", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newbudgetform", function () {
        cy.visit("/cgi-bin/koha/admin/aqbudgetperiods.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("newbudgetform");
    });
});
