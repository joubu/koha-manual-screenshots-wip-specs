describe("admin/cataloging/frameworks", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("frameworks", function () {
        cy.visit("/cgi-bin/koha/admin/biblio_framework.pl");
        cy.get("main").should('be.visible').screenshot("frameworks");
    });
});
