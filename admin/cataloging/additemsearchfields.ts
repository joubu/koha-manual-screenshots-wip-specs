describe("admin/cataloging/additemsearchfields", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("additemsearchfields", function () {
        cy.visit("/cgi-bin/koha/admin/items_search_field.pl");
        cy.get("main").should('be.visible').screenshot("additemsearchfields");
    });
});
