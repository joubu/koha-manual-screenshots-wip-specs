describe("admin/cataloging/editfield", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("editfield", function () {
        cy.visit("/cgi-bin/koha/admin/marctagstructure.pl?op=add_form&searchfield=020&frameworkcode=BKS");
        cy.get("fieldset.rows").should('be.visible').screenshot("editfield");
    });
});
