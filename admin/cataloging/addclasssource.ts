describe("admin/cataloging/addclasssource", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addclasssource", function () {
        cy.visit("/cgi-bin/koha/admin/classsources.pl?op=add_source");
        cy.get("main").should('be.visible').screenshot("addclasssource");
    });
});
