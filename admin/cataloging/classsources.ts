describe("admin/cataloging/classsources", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("classsources", function () {
        cy.visit("/cgi-bin/koha/admin/classsources.pl");
        cy.get("main").should('be.visible').screenshot("classsources");
    });
});
