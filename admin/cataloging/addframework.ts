describe("admin/cataloging/addframework", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addframework", function () {
        cy.visit("/cgi-bin/koha/admin/biblio_framework.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("addframework");
    });
});
