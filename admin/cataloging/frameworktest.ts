describe("admin/cataloging/frameworktest", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("frameworktest", function () {
        cy.visit("/cgi-bin/koha/admin/checkmarc.pl");
        cy.get("main").should('be.visible').screenshot("frameworktest");
    });
});
