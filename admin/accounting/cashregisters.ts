describe("admin/accounting/cashregisters", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("cashregisters", function () {
        cy.set_syspref("UseCashRegisters", "Use");
        cy.visit("/cgi-bin/koha/admin/cash_registers.pl");
        /* FIXME - Invalid step  */
        /* FIXME - Invalid step Enable UseCashRegisters, go to Administration > Cash registers > New cash registers, add cash registers */
        cy.get("main").should('be.visible').screenshot("cashregisters");
    });
});
