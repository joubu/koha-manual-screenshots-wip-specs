describe("admin/accounting/alldebittypes", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("alldebittypes", function () {
        cy.visit("/cgi-bin/koha/admin/debit_types.pl");
        cy.get("#filter_system").click();
        cy.get("main").should('be.visible').screenshot("alldebittypes");
    });
});
