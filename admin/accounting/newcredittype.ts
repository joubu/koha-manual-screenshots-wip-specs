describe("admin/accounting/newcredittype", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newcredittype", function () {
        cy.visit("/cgi-bin/koha/admin/credit_types.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("newcredittype");
    });
});
