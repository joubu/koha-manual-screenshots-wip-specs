describe("admin/accounting/newdebittype", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newdebittype", function () {
        cy.visit("/cgi-bin/koha/admin/debit_types.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("newdebittype");
    });
});
