describe("admin/accounting/credittypes", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("credittypes", function () {
        cy.visit("/cgi-bin/koha/admin/credit_types.pl");
        cy.get("#filter_system").click();
        cy.get("main").should('be.visible').screenshot("credittypes");
    });
});
