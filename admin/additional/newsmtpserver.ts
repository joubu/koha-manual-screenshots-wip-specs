describe("admin/additional/newsmtpserver", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newsmtpserver", function () {
        cy.visit("/cgi-bin/koha/admin/smtp_servers.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("newsmtpserver");
    });
});
