describe("admin/additional/addfield", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addfield", function () {
        cy.visit("/cgi-bin/koha/admin/additional-fields.pl?op=add_form&tablename=subscription");
        cy.get("main").should('be.visible').screenshot("addfield");
    });
});
