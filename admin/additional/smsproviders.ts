describe("admin/additional/smsproviders", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("smsproviders", function () {
        cy.set_syspref("SMSSendDriver", "Email");
        cy.visit("/cgi-bin/koha/admin/sms_providers.pl");
        cy.visit("/cgi-bin/koha/admin/sms_providers.pl");
        cy.get("#new_provider").click();
        /* FIXME - Invalid step xxx */
        cy.get("main").should('be.visible').screenshot("smsproviders");
    });
});
