describe("admin/additional/smtpservers", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("smtpservers", function () {
        cy.visit("/cgi-bin/koha/admin/smtp_servers.pl");
        cy.get("main").should('be.visible').screenshot("smtpservers");
    });
});
