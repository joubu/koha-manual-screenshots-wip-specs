describe("admin/additional/newsru", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newsru", function () {
        cy.visit("/cgi-bin/koha/admin/z3950servers.pl?op=add&type=sru");
        cy.get("main").should('be.visible').screenshot("newsru");
    });
});
