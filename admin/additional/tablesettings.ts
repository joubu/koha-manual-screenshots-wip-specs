describe("admin/additional/tablesettings", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("tablesettings", function () {
        cy.visit("/cgi-bin/koha/admin/columns_settings.pl");
        cy.get("main").should('be.visible').screenshot("tablesettings");
    });
});
