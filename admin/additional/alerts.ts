describe("admin/additional/alerts", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("alerts", function () {
        cy.set_syspref("AudioAlerts", "1");
        cy.visit("/cgi-bin/koha/admin/audio_alerts.pl");
        cy.get("main").should('be.visible').screenshot("alerts");
    });
});
