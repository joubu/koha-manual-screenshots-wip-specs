describe("admin/patroncirc/transportcostmatrix", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("transportcostmatrix", function () {
        cy.visit("/cgi-bin/koha/admin/transport-cost-matrix.pl");
        cy.get("#transport-cost-matrix table").should('be.visible').screenshot("transportcostmatrix");
    });
});
