describe("admin/basicparams/additemtype", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("additemtype", function () {
        cy.visit("/cgi-bin/koha/admin/itemtypes.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("additemtype");
    });
});
