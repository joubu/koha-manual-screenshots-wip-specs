describe("admin/basicparams/librarydetails", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("librarydetails", function () {
        cy.visit("/cgi-bin/koha/admin/branches.pl?op=view&branchcode=MPL");
        cy.get("main").should('be.visible').screenshot("librarydetails");
    });
});
