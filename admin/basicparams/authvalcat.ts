describe("admin/basicparams/authvalcat", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("authvalcat", function () {
        cy.visit("/cgi-bin/koha/admin/authorised_values.pl");
        cy.get("main").should('be.visible').screenshot("authvalcat");
    });
});
