describe("admin/basicparams/addauthcat", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addauthcat", function () {
        cy.visit("/cgi-bin/koha/admin/authorised_values.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("addauthcat");
    });
});
