describe("admin/basicparams/newdesk", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newdesk", function () {
        cy.set_syspref("UseCirculationDesks", "1");
        cy.visit("/cgi-bin/koha/admin/desks.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("newdesk");
    });
});
