describe("admin/basicparams/newitemtype", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newitemtype", function () {
        cy.visit("/cgi-bin/koha/admin/itemtypes.pl");
        cy.get("#table_item_type").should('be.visible').screenshot("newitemtype");
    });
});
