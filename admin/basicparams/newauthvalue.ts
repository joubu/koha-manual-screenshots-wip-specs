describe("admin/basicparams/newauthvalue", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newauthvalue", function () {
        cy.visit("/cgi-bin/koha/admin/authorised_values.pl?op=add_form&category=CCODE");
        cy.get("main").should('be.visible').screenshot("newauthvalue");
    });
});
