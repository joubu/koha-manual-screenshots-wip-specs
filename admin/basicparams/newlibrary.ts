describe("admin/basicparams/newlibrary", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newlibrary", function () {
        cy.visit("/cgi-bin/koha/admin/branches.pl?op=add_form");
        cy.get("#Aform > :nth-child(1)").should('be.visible').screenshot("newlibrary");
    });
});
