describe("patrons/addaltaddress", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addaltaddress", function () {
        cy.visit("/cgi-bin/koha/members/memberentry.pl?op=add&categorycode=PT");
        cy.get("#memberentry_altaddress").should('be.visible').screenshot("addaltaddress");
    });
});
