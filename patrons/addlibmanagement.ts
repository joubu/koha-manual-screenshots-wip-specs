describe("patrons/addlibmanagement", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addlibmanagement", function () {
        cy.visit("/cgi-bin/koha/members/memberentry.pl?op=add&categorycode=PT");
        cy.get("#memberentry_library_management").should('be.visible').screenshot("addlibmanagement");
    });
});
