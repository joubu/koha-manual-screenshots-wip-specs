describe("searching/searchresults", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("searchresults", function () {
        cy.visit("/cgi-bin/koha/authorities/detail-biblio-search.pl?q=shakespeare");
        cy.get("main").should('be.visible').screenshot("searchresults");
    });
});
