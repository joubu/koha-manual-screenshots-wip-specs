describe("staffinterface/news", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("news", function () {
        cy.visit("/cgi-bin/koha/tools/additional-contents.pl?category=news");
        cy.get("main").should('be.visible').screenshot("news");
    });
});
