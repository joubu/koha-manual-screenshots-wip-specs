describe("about/aboutserver", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("aboutserver", function () {
        cy.visit("/cgi-bin/koha/about.pl#about_panel");
        cy.get("#about_panel").should('be.visible').screenshot("aboutserver");
    });
});
