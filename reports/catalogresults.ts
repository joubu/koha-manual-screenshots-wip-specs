describe("reports/catalogresults", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("catalogresults", function () {
        cy.visit("/cgi-bin/koha/reports/catalogue_stats.pl");
        cy.get("main").should('be.visible').screenshot("catalogresults");
    });
});
