describe("cataloging/catalogsearch", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("catalogsearch", function () {
        cy.visit("/cgi-bin/koha/cataloguing/addbooks.pl?q=paris");
        cy.get(".searchresults > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(4) > div:nth-child(1)").click();
        cy.get(".open > ul:nth-child(2) > li:nth-child(3)").should('be.visible').screenshot("catalogsearch");
    });
});
