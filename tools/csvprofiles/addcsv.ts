describe("tools/csvprofiles/addcsv", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addcsv", function () {
        cy.visit("/cgi-bin/koha/tools/csv-profiles.pl?op=add_form");
        cy.get("main").should('be.visible').screenshot("addcsv");
    });
});
