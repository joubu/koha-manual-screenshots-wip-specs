describe("tools/additionalcontent/addnews", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addnews", function () {
        cy.visit("/cgi-bin/koha/tools/additional-contents.pl?op=add_form&category=news&editmode=wysiwyg");
        cy.get("main").should('be.visible').screenshot("addnews");
    });
});
