describe("tools/additionalcontent/addadditionalcontent", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addadditionalcontent", function () {
        cy.visit("/cgi-bin/koha/tools/additional-contents.pl?op=add_form&category=html_customizations&editmode=wysiwyg");
        cy.get("main").should('be.visible').screenshot("addadditionalcontent");
    });
});
