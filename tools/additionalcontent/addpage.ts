describe("tools/additionalcontent/addpage", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addpage", function () {
        cy.visit("/cgi-bin/koha/tools/additional-contents.pl?op=add_form&category=pages&editmode=wysiwyg");
        cy.get("main").should('be.visible').screenshot("addpage");
    });
});
