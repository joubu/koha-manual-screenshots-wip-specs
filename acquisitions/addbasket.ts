describe("acquisitions/addbasket", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("addbasket", function () {
        cy.visit("/cgi-bin/koha/acqui/basketheader.pl?booksellerid=1&op=add_form");
        cy.get(".main").should('be.visible').screenshot("addbasket");
    });
});
