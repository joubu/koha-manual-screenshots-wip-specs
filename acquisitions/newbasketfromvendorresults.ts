describe("acquisitions/newbasketfromvendorresults", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newbasketfromvendorresults", function () {
        cy.visit("/cgi-bin/koha/acqui/booksellers.pl");
        cy.visit("/cgi-bin/koha/");
        cy.get(".dropdown-menu").should('be.visible').screenshot("newbasketfromvendorresults");
    });
});
