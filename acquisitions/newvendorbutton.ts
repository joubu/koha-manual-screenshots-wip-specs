describe("acquisitions/newvendorbutton", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newvendorbutton", function () {
        cy.visit("/cgi-bin/koha/acqui/acqui-home.pl");
        cy.get("#toolbar").should('be.visible').screenshot("newvendorbutton");
    });
});
