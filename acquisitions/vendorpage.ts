describe("acquisitions/vendorpage", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("vendorpage", function () {
        cy.visit("/cgi-bin/koha/acqui/supplier.pl?booksellerid=1");
        cy.get("main").should('be.visible').screenshot("vendorpage");
    });
});
