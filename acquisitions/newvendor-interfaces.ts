describe("acquisitions/newvendor-interfaces", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newvendor-interfaces", function () {
        cy.visit("/cgi-bin/koha/acqui/supplier.pl?op=enter");
        /* FIXME - Invalid step add_interface() */
        cy.get("#supplier-interface").should('be.visible').screenshot("newvendor-interfaces");
    });
});
