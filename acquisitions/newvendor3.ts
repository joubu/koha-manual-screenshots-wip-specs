describe("acquisitions/newvendor3", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newvendor3", function () {
        cy.visit("/cgi-bin/koha/acqui/supplier.pl?op=enter");
        cy.get("#ordering_information").should('be.visible').screenshot("newvendor3");
    });
});
