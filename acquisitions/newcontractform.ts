describe("acquisitions/newcontractform", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newcontractform", function () {
        cy.visit("/cgi-bin/koha/admin/aqcontract.pl?op=add_form&booksellerid=1");
        cy.get("main").should('be.visible').screenshot("newcontractform");
    });
});
