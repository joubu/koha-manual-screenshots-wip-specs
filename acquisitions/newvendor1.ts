describe("acquisitions/newvendor1", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("newvendor1", function () {
        cy.visit("/cgi-bin/koha/acqui/supplier.pl?op=enter");
        cy.get("#company_details").should('be.visible').screenshot("newvendor1");
    });
});
