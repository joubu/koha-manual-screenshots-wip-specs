describe("acquisitions/invoicesearch", function () {
    beforeEach(() => {
        cy.login();
        cy.set_cookie_lang();
    });

    it("invoicesearch", function () {
        cy.visit("/cgi-bin/koha/acqui/invoices.pl");
        cy.get("class=\"row\"").should('be.visible').screenshot("invoicesearch");
    });
});
